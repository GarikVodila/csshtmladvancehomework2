'use strict'

const { src, dest, watch, task, lastRun, series, parallel } = require('gulp');
const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCss = require('gulp-clean-css');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const jsMinify = require('gulp-js-minify');
const fileInclude = require('gulp-file-include');
const prettyHtml = require('gulp-pretty-html');



task('html', () => {
	return src('src/*.html', {since: lastRun('html')})
		.pipe(fileInclude())
		.pipe(prettyHtml({
			indent_size: 4,
			indent_char: ' ',
			unformatted: ['code', 'pre', 'em', 'strong', 'span', 'i', 'b', 'br']
		}))
		.pipe(dest('dist'))
		.pipe(browserSync.reload({ stream: true }));
});

task('clean', () => {
  return src('dist/*', {read: false})
		.pipe(clean());
});

task('styles', () => {
  return src('src/styles/**/*.scss', {since: lastRun('styles')})
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(autoprefixer())
      .pipe(concat('styles.min.css'))
      .pipe(cleanCss({
        level: 2,
        format: 'keep-breaks'}))
      .pipe(sourcemaps.write('.'))
      .pipe(dest('dist/css'))
      .pipe(browserSync.reload({ stream: true })); 
});


task('scripts', () => {
  return src('src/scripts/**/*.js', {since: lastRun('scripts')})
  .pipe(sourcemaps.init())
  .pipe(concat('scripts.min.js'))
  .pipe(jsMinify())
  .pipe(sourcemaps.write('.'))
  .pipe(dest('dist/js'))
  .pipe(browserSync.reload({ stream: true })); 
});

task('images', () => {
	return src('src/img/**/*.+(png|jpg|jpeg|svg)', {since: lastRun('images')})
  .pipe(imagemin({
    progressive: true
  }))
  .pipe(dest('dist/img'))
  .pipe(browserSync.reload({ stream: true })); 
});

task('serve', () => {
	return browserSync.init({
		server: {
			baseDir: ['./dist/']
		},
		port: 9000,
		open: true
	});
});

task('watchers', () => {
	watch('./src/styles/**/*.scss', parallel('styles')).on('change', browserSync.reload);
	watch('./src/scripts/**/*.js', parallel('scripts')).on('change', browserSync.reload);
	watch('./src/**/*.html', parallel('html')).on('change', browserSync.reload);
	watch('./src/img/**/*.+(png|jpg|jpeg|svg)', series('images')).on('change', browserSync.reload);
	watch('./src/**/*.html', series('html')).on('change', browserSync.reload);
});

task('build', series('clean', 'html', parallel('styles', 'scripts', 'images')));

task('dev', parallel('serve', 'watchers'));

task('default', series('build', parallel('serve', 'watchers')));


